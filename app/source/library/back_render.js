
var BackRenderer = function (Paper, blind, paperElement) {
	var paper = new Paper.PaperScope();
	paper.setup(document.getElementById(paperElement));
	
	var outline = {};
	
	var view = {
		decorations: {},
		folds: []
	};
	
	var setOutline = function () {
		outline = {
			width: blind.get().width,
			height: blind.get().height - blind.get().tailHeight,
			position: {
				left: blind.get().left,
				right: blind.get().right,
				top: blind.get().top,
				bottom: blind.get().bottom,
				center: {
					x: BLIND_CENTER.x,
					y: BLIND_CENTER.y - (blind.get().tailHeight / 2)
				}
			}
		};
	};
	var renderOutline = function () {
		if (view.outline) {
			view.outline.remove();
		}
		view.outline = new paper.Shape.Rectangle({
			center: outline.position.center,
			strokeColor: BACK_OUTLINE_COLOR,
			size: {width: outline.width, height: outline.height}
		});
		if (view.outlineSeamLeft) {
			view.outlineSeamLeft.remove();
		}
		view.outlineSeamLeft = new paper.Path({
			segments: [
				[outline.position.left + blind.get().frontWidthOffset, outline.position.top],
				[outline.position.left + blind.get().frontWidthOffset, outline.position.bottom]
			],
			dashArray: [7, 5],
			strokeColor: BACK_OUTLINE_COLOR
		});
		if (view.outlineSeamRight) {
			view.outlineSeamRight.remove();
		}
		view.outlineSeamRight = new paper.Path({
			segments: [
				[outline.position.right - blind.get().frontWidthOffset, outline.position.top],
				[outline.position.right - blind.get().frontWidthOffset, outline.position.bottom]
			],
			dashArray: [7, 5],
			strokeColor: BACK_OUTLINE_COLOR
		});
	};
	var renderDecorations = function () {
		if (view.decorations.width) {
			view.decorations.width.remove();
		}
		view.decorations.width = new paper.PointText({
			point: [outline.position.center.x, outline.position.center.y - (outline.height / 2) - (FONT_SIZE / 2)],
			content: outline.width,
			fillColor: BACK_OUTLINE_COLOR,
			fontFamily: 'Courier New',
			fontSize: FONT_SIZE
		});
		if (view.decorations.height) {
			view.decorations.height.remove();
		}
		view.decorations.height = new paper.PointText({
			point: [outline.position.center.x - (outline.width / 2), outline.position.center.y],
			content: outline.height,
			fillColor: BACK_OUTLINE_COLOR,
			fontFamily: 'Courier New',
			fontSize: FONT_SIZE
		}).rotate(270);
		
		if (view.decorations.fold) {
			view.decorations.fold.remove();
		}
		view.decorations.fold = new paper.PointText({
			point: [outline.position.center.x, outline.position.center.y + (outline.height / 2) - ((blind.get().foldOffset - FONT_SIZE) / 2)],
			content: blind.get().foldOffset.toFixed(1),
			fillColor: BACK_OUTLINE_COLOR,
			fontFamily: 'Courier New',
			fontSize: FONT_SIZE
		});
		
		if (view.decorations.foldWidth) {
			view.decorations.foldWidth.remove();
		}
		view.decorations.foldWidth = new paper.PointText({
			point: [outline.position.center.x, outline.position.center.y + (outline.height / 2) - (blind.get().foldOffset * 3) - (FONT_SIZE / 2)],
			content: (blind.get().width - (2 * (blind.get().frontWidthOffset + blind.get().seam))).toFixed(0),
			fillColor: BACK_OUTLINE_COLOR,
			fontFamily: 'Courier New',
			fontSize: FONT_SIZE
		});
		
		if (view.decorations.widthOffset) {
			view.decorations.widthOffset.remove();
		}
		view.decorations.widthOffset = new paper.PointText({
			point: [outline.position.left + (FONT_SIZE / 2), outline.position.top + (FONT_SIZE) + blind.get().loopMountHeight],
			content: blind.get().frontWidthOffset,
			fillColor: BACK_OUTLINE_COLOR,
			fontFamily: 'Courier New',
			fontSize: FONT_SIZE
		});
	};
	
	var renderFolds = function () {
		blind.get().foldOffset = (blind.get().height - blind.get().loopMountHeight - blind.get().tailHeight) / blind.get().foldCount;
		for(var i = 0; i < view.folds.length; i++) {
			if (view.folds[i]) {
				view.folds[i].remove();
			}
		}
		for(var i = 0; i < blind.get().foldCount; i++) {
			view.folds[i] = new paper.Path.Line({
				from: [
					blind.get().left + Math.abs(blind.get().frontWidthOffset + blind.get().seam), 
					blind.get().top + ((i+1) * blind.get().foldOffset) + blind.get().loopMountHeight
				],
				to: [
					blind.get().right - Math.abs(blind.get().frontWidthOffset + blind.get().seam), 
					blind.get().top + ((i+1) * blind.get().foldOffset) + blind.get().loopMountHeight
				],
				strokeColor: BACK_OUTLINE_COLOR,
				dashArray: i % 2 === 0 ? [10, 13] : []
			});
		}
	};
	
	var updateStrokeWidth = function () {
		var strokeWidthBase = 1 / paper.view.zoom;
		view.outline.set({strokeWidth: strokeWidthBase});
	};
	
	var autoZoom = function () {
		if (
			blind.get().width * 1.05 > paper.view.bounds.width
			|| 
				(
					blind.get().height
					+ (2.2 * blind.get().loopMountHeight) 
					+ (2.2 * blind.get().tailHeight)
				) * 1.2 
				> paper.view.bounds.height
			
		) {
			paper.view.zoom *= 0.97;
			return autoZoom();
			
		} else if (
			blind.get().width * 1.2 < paper.view.bounds.width
			&& (
				(
					blind.get().height
					+ (2,2 * blind.get().loopMountHeight) 
					+ (2.2 * blind.get().tailHeight)
				) * 1.4 
				< paper.view.bounds.height
			)
		) {
			paper.view.zoom *= 1.05;
			return autoZoom();
		}
	};
	
	this.get = function() {
		return properties;
	};
	
	this.render = function () {
		window.paper = paper;
		setOutline();			
		renderOutline();
		renderFolds();
		renderDecorations();
		updateStrokeWidth();
		autoZoom();
	};
};
