/*
 * The Blind Renderer
 * Uses: Paperjs as renderer
 */
 
var BlindRenderer = function (Paper, blind, paperElement) {
	var paper = new Paper.PaperScope();
	paper.setup(document.getElementById(paperElement));
	
	window.paper = paper;
	var view = {
		outline: new paper.Shape.Rectangle({
			center: BLIND_CENTER,
			strokeColor: OUTLINE_COLOR,
			fillColor: FILL_COLOR,
			size: {width: 100, height: 40}
		})
	};
	
	var renderLoopMount = function () {
		if (view.loopMount) {
			view.loopMount.remove();
		}
		view.loopMount = new paper.Path.Line({
			from: [
				blind.get().left, 
				blind.get().top + blind.get().loopMountHeight
			],
			to: [
				blind.get().right, 
				blind.get().top + blind.get().loopMountHeight
			],
			strokeColor: OUTLINE_COLOR
		});
	};
	
	var renderTail = function () {
		if (view.tail) {
			view.tail.remove();
		}
		view.tail = new paper.Path.Line({
			from: [
				blind.get().left, 
				blind.get().bottom - blind.get().tailHeight
			],
			to: [
				blind.get().right, 
				blind.get().bottom - blind.get().tailHeight
			],
			strokeColor: OUTLINE_COLOR
		});
	};
	var updateStrokeWidth = function() {
		var strokeWidthBase = 1 / paper.view.zoom;
		view.outline.set({strokeWidth: strokeWidthBase});
		view.loopMount.set({strokeWidth: strokeWidthBase});
		view.tail.set({strokeWidth: strokeWidthBase});
		$.each(view.folds, function (i, fold) {
			var foldStrokeWidth = strokeWidthBase / (2 + (((i+1)%2)*2));
			fold.set({strokeWidth: foldStrokeWidth});
		});
	};
	var autoZoom = function () {
		if (
			blind.get().width * 1.05 > paper.view.bounds.width
			|| 
				(
					blind.get().height
					+ (2.2 * blind.get().loopMountHeight) 
					+ (2.2 * blind.get().tailHeight)
				) * 1.2 
				> paper.view.bounds.height
			
		) {
			paper.view.zoom *= 0.97;
			return autoZoom();
			
		} else if (
			blind.get().width * 1.2 < paper.view.bounds.width
			&& (
				(
					blind.get().height
					+ (2.2 * blind.get().loopMountHeight) 
					+ (2.2 * blind.get().tailHeight)
				) * 1.4 
				< paper.view.bounds.height
			)
		) {
			paper.view.zoom *= 1.05;
			return autoZoom();
		}
	};
	// Update the view
	this.render = function () {
		window.paper = paper;
		view.outline.remove()
		
		view.outline = new paper.Shape.Rectangle({
			center: BLIND_CENTER,
			strokeColor: OUTLINE_COLOR,
			fillColor: FILL_COLOR,
			size: {
				height: blind.get().height,
				width: blind.get().width
			}
		});
		renderLoopMount();
		renderTail();
		updateStrokeWidth();
		autoZoom();
	};
};
