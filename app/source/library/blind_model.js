/*
 * The Blind model caputuring its properties
 */

var Blind = function () {
	// Properties
	var properties = {
		width: 0,
		height: 0,
		foldOffset: 0,
		foldCount: 7,
		frontWidthOffset: 0,
		seam: 10,
		tailHeight: 0,
		loopMountHeight: 0,
		top: 0,
		left: 0,
		bottom: 0,
		right: 0
	};
	
	// Setter functions
	this.setWidth = function (e) {
		properties.width = parseInt(e.target.value, 10);
	};
	this.setFoldCount = function (e) {
		properties.foldCount = parseInt(e.target.value, 10);
	};
	this.setHeight = function (e) {
		properties.height = parseInt(e.target.value, 10);
	};
	this.setLoopMountHeight = function (e) {
		properties.loopMountHeight = parseInt(e.target.value, 10);
	};
	this.setSeam = function (e) {
		properties.seam = parseInt(e.target.value, 10);
	};
	this.setTailHeight = function (e) {
		properties.tailHeight = parseInt(e.target.value, 10);
		if (properties.tailHeight === 0) {
			properties.tailHeight = (properties.height - properties.loopMountHeight) / (properties.foldCount + 1)
		}
	};
	this.setFrontWidthOffset = function (e) {
		properties.frontWidthOffset = parseInt(e.target.value, 10);
	};
	
	this.get = function () {
		properties.top = BLIND_CENTER.y - (properties.height/2);
		properties.bottom = BLIND_CENTER.y + (properties.height/2);
		properties.left = BLIND_CENTER.x - (properties.width/2);
		properties.right = BLIND_CENTER.x + (properties.width/2);
		return properties;
	};
};
