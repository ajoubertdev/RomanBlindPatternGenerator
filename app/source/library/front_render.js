

var FrontRenderer = function (Paper, blind, paperElement) {
	var paper = new Paper.PaperScope();
	paper.setup(document.getElementById(paperElement));
		
	var properties = {
		body: {},
		looping: {}
	};
	var view = {
		decorations: {}
	};
	
	// Looping extension
	var setLooping = function() {
		properties.looping = {
			height: blind.get().loopMountHeight * 2,
			width: blind.get().width + (2 * blind.get().seam),
			position: {
				top: blind.get().top - (blind.get().loopMountHeight * 2),
				left: blind.get().left - blind.get().seam,
				right: blind.get().right + blind.get().seam,
				bottom: blind.get().top
			}
		};
	};
	var renderLooping = function() {
		if (view.looping) {
			view.looping.remove();
		}
		view.looping = new paper.Path({
			segments: [
				[properties.looping.position.left, properties.looping.position.bottom],
				[properties.looping.position.left, properties.looping.position.top],
				[properties.looping.position.right, properties.looping.position.top],
				[properties.looping.position.right, properties.looping.position.bottom]
			],
			strokeColor: FRONT_OUTLINE_COLOR
		});
		if (view.loopingSeamLeft) {
			view.loopingSeamLeft.remove();
		}
		view.loopingSeamLeft = new paper.Path({
			segments: [
				[properties.looping.position.left + blind.get().seam, properties.looping.position.bottom],
				[properties.looping.position.left + blind.get().seam, properties.looping.position.top]
			],
			dashArray: [5, 15],
			strokeColor: FRONT_OUTLINE_COLOR
		});
		if (view.loopingSeamRight) {
			view.loopingSeamRight.remove();
		}
		view.loopingSeamRight = new paper.Path({
			segments: [
				[properties.looping.position.right - blind.get().seam, properties.looping.position.bottom],
				[properties.looping.position.right - blind.get().seam, properties.looping.position.top]
			],
			dashArray: [5, 15],
			strokeColor: FRONT_OUTLINE_COLOR
		});
	};
	// Sides extension
	var setBody = function () {
		properties.body = {
			height: blind.get().height + (blind.get().tailHeight * 2),
			width: ((blind.get().frontWidthOffset + blind.get().seam) * 2) + blind.get().width,
			position: {
				top: blind.get().top,
				left: blind.get().left - (blind.get().frontWidthOffset * 2),
				right: blind.get().right + (blind.get().frontWidthOffset * 2),
				bottom: blind.get().top + blind.get().height + blind.get().tailHeight + blind.get().seam
			}
		};
	};
	var renderBody = function () {
		if (view.body) {
			view.body.remove();
		}
		view.body = new paper.Path({
			segments: [
				[blind.get().left, properties.body.position.top],
				[properties.body.position.left, properties.body.position.top],
				[properties.body.position.left, properties.body.position.bottom + blind.get().seam],
				[properties.body.position.right, properties.body.position.bottom + blind.get().seam],
				[properties.body.position.right, properties.body.position.top],
				[blind.get().right, properties.body.position.top]
			],
			strokeColor: FRONT_OUTLINE_COLOR
		});
		if (view.bodySeamLeft) {
			view.bodySeamLeft.remove();
		}
		view.bodySeamLeft = new paper.Path({
			segments: [
				[properties.body.position.left + blind.get().frontWidthOffset, properties.body.position.top],
				[properties.body.position.left + blind.get().frontWidthOffset, properties.body.position.bottom]
			],
			dashArray: [7, 7],
			strokeColor: FRONT_OUTLINE_COLOR
		});
		if (view.bodySeamRight) {
			view.bodySeamRight.remove();
		}
		view.bodySeamRight = new paper.Path({
			segments: [
				[properties.body.position.right - blind.get().frontWidthOffset, properties.body.position.top],
				[properties.body.position.right - blind.get().frontWidthOffset, properties.body.position.bottom]
			],
			dashArray: [7, 7],
			strokeColor: FRONT_OUTLINE_COLOR
		});
		if (view.bodySeamTail) {
			view.bodySeamTail.remove();
		}
		view.bodySeamTail = new paper.Path({
			segments: [
				[properties.body.position.left + blind.get().frontWidthOffset , properties.body.position.bottom],
				[properties.body.position.right - blind.get().frontWidthOffset, properties.body.position.bottom]
			],
			dashArray: [5, 15],
			strokeColor: FRONT_OUTLINE_COLOR
		});
	};
	var renderDecorations = function () {
		if (view.decorations.loopMountHeight) {
			view.decorations.loopMountHeight.remove();
		}
		view.decorations.loopMountHeight = new paper.PointText({
			point: [properties.looping.position.left - (FONT_SIZE / 2) - blind.get().seam, properties.looping.position.top + (properties.looping.height / 2)],
			content: properties.looping.height,
			fillColor: FRONT_OUTLINE_COLOR,
			fontFamily: 'Courier New',
			fontSize: FONT_SIZE
		}).rotate(270);
		
		if (view.decorations.leftSide) {
			view.decorations.leftSide.remove();
		}
		view.decorations.leftSide = new paper.PointText({
			point: [properties.body.position.left - (FONT_SIZE * 2), properties.body.position.top + (properties.body.height / 2)],
			content: properties.body.height,
			fillColor: FRONT_OUTLINE_COLOR,
			fontFamily: 'Courier New',
			fontSize: FONT_SIZE
		}).rotate(270);
		
		if (view.decorations.bottom) {
			view.decorations.bottom.remove();
		}
		view.decorations.bottom = new paper.PointText({
			point: [properties.body.position.left + (properties.body.width / 2), properties.body.position.bottom + (FONT_SIZE * 2)],
			content: properties.body.width,
			fillColor: FRONT_OUTLINE_COLOR,
			fontFamily: 'Courier New',
			fontSize: FONT_SIZE
		});
		
		if (view.decorations.top) {
			view.decorations.top.remove();
		}
		view.decorations.top = new paper.PointText({
			point: [properties.looping.position.left + (properties.looping.width / 2), properties.looping.position.top - (FONT_SIZE / 2)],
			content: properties.looping.width,
			fillColor: FRONT_OUTLINE_COLOR,
			fontFamily: 'Courier New',
			fontSize: FONT_SIZE
		});
		
		if (view.decorations.widthOffset) {
			view.decorations.widthOffset.remove();
		}
		view.decorations.widthOffset = new paper.PointText({
			point: [properties.body.position.left + (FONT_SIZE / 2), properties.body.position.top + (FONT_SIZE)],
			content: blind.get().frontWidthOffset,
			fillColor: FRONT_OUTLINE_COLOR,
			fontFamily: 'Courier New',
			fontSize: FONT_SIZE
		});
	};
	var updateStrokeWidth = function() {
		var strokeWidthBase = 1 / paper.view.zoom;
		view.looping.set({strokeWidth: strokeWidthBase});
		view.body.set({strokeWidth: strokeWidthBase});
	};
	
	this.get = function() {
		return properties;
	};
	
	var autoZoom = function () {
		if (
			blind.get().width * 1.05 > paper.view.bounds.width
			|| 
				(
					blind.get().height
					+ (2.2 * blind.get().loopMountHeight) 
					+ (2.2 * blind.get().tailHeight)
				) * 1.2 
				> paper.view.bounds.height
			
		) {
			paper.view.zoom *= 0.97;
			return autoZoom();
			
		} else if (
			blind.get().width * 1.2 < paper.view.bounds.width
			&& (
				(
					blind.get().height
					+ (2.2 * blind.get().loopMountHeight) 
					+ (2.2 * blind.get().tailHeight)
				) * 1.4 
				< paper.view.bounds.height
			)
		) {
			paper.view.zoom *= 1.05;
			return autoZoom();
		}
	};
	
	this.render = function() {
		window.paper = paper;
		setLooping();
		renderLooping();
		
		setBody();
		renderBody();
		
		updateStrokeWidth();
		renderDecorations();
		autoZoom();
	};
};
