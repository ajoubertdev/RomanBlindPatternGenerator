var BACK_OUTLINE_COLOR = new paper.Color(119/254, 204/254, 249/254, 1);
var FILL_COLOR = new paper.Color(1, 1, 1, 0);
var OUTLINE_COLOR = new paper.Color(0, 0, 0, 1);
var FRONT_OUTLINE_COLOR = new paper.Color(1, 0.4, 1, 1);
var BLIND_CENTER = {x: parseInt($('canvas')[0].width / 2, 10), y: parseInt($('canvas')[0].height / 2, 10)};
var FONT_SIZE = 25;
var blind = new Blind();
var blindRenderer = new BlindRenderer(paper, blind, 'pattern');
var backRenderer = new BackRenderer(paper, blind, 'back');
var frontRenderer = new FrontRenderer(paper, blind, 'front');

$(document).ready(function () {	
	/**
	 * BINDINGS
	 */
	$('#blindWidth').on('blur', blind.setWidth).on('change', blind.setWidth);
	$('#blindHeight').on('blur', blind.setHeight).on('change', blind.setHeight);
	$('#loopMountHeight').on('blur', blind.setLoopMountHeight).on('change', blind.setLoopMountHeight);
	$('#seam').on('blur', blind.setSeam).on('change', blind.setSeam);
	$('#tailHeight').on('blur', blind.setTailHeight).on('change', blind.setTailHeight);
	$('#foldCount').on('blur', blind.setFoldCount).on('change', blind.setFoldCount);
	$('#frontWidthOffset').on('blur', blind.setFrontWidthOffset).on('change', blind.setFrontWidthOffset);

	$('input,select').on('blur', blindRenderer.render).on('change', blindRenderer.render);
	$('input,select').on('blur', frontRenderer.render).on('change', frontRenderer.render);
	$('input,select').on('blur', backRenderer.render).on('change', backRenderer.render);

	$(window).resize(function () {
		BLIND_CENTER.x = parseInt($('canvas')[0].width / 3, 10);
		BLIND_CENTER.y = parseInt($('canvas')[0].height / 3.5, 10);
		console.log(BLIND_CENTER);
		blindRenderer.render();
		frontRenderer.render();
		backRenderer.render();	
	});
	/**
	 * Triggers
	 */
	$('input').trigger('blur');
	$(window).trigger('resize');
});
