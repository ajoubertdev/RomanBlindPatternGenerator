#Roman Blind Pattern Generator

Everything you ever wanted from a tool that can help generate roman blind patterns for lined roman blinds. Looking to be offline friendly to produce a scaled pattern for both front and lining of the blind.

Current features:

- Read me

Upcoming Features

- Enter in blind dimensions
- Enter in seam and other blind specific dimensions
- Preview area of blind
- Printable (scaled) pattern for front
- Printable (scaled) pattern for lining